package com.example.threescreens

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class btn_with_text : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_btn_with_text)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Besty"
    }
}