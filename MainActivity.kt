package com.example.threescreens

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout


class MainActivity : AppCompatActivity() {
    private lateinit var btn_transfer: Button
    private lateinit var ed_first_name: EditText
    private lateinit var ed_second_name: EditText
    private lateinit var ed_third_name: EditText
    private lateinit var ed_age: EditText
    private lateinit var ed_hobby: EditText
    private lateinit var first_name_save: TextView
    private lateinit var second_name_save: TextView
    private lateinit var third_name_save: TextView
    private lateinit var age_save: TextView
    private lateinit var hobby_save: TextView
    private lateinit var layout: ConstraintLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Besty"

        setContentView(R.layout.activity_main)
        layout = findViewById(R.id.ConstraintLayout)
        btn_transfer = findViewById(R.id.btn_transfer)
        ed_first_name = findViewById(R.id.ed_first_name)
        ed_second_name = findViewById(R.id.ed_second_name)
        ed_third_name = findViewById(R.id.ed_third_name)
        ed_age = findViewById(R.id.ed_age)
        ed_hobby = findViewById(R.id.ed_hobby)

        first_name_save = findViewById(R.id.first_name_save)
        second_name_save = findViewById(R.id.second_name_save)
        third_name_save = findViewById(R.id.third_name_save)
        age_save = findViewById(R.id.age_save)
        hobby_save = findViewById(R.id.hobby_save)

        loadData()

        btn_transfer.setOnClickListener {
            intent = Intent(this, InfoActivity::class.java)
            intent.putExtra("first", ed_first_name.text.toString())
            intent.putExtra("second", ed_second_name.text.toString())
            intent.putExtra("third", ed_third_name.text.toString())
            intent.putExtra("age", ed_age.text.toString())
            intent.putExtra("hobby", ed_hobby.text.toString())
            startActivity(intent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.save -> saveData()
            R.id.background -> changeBack()
            R.id.screen -> Intent(this, btn_with_text::class.java).also { startActivity(it) }
        }
        return true
    }

    private fun changeBack() {
        when ((1..3).random()) {
            1 -> layout.setBackgroundResource(R.drawable.bg1)
            2 -> layout.setBackgroundResource(R.drawable.bg2)
            3 -> layout.setBackgroundResource(R.drawable.bg3)
        }
    }

    private fun saveData() {
        val insertedText = listOf(ed_first_name.text.toString(), ed_second_name.text.toString(), ed_third_name.text.toString(), ed_age.text.toString(), ed_hobby.text.toString())
        first_name_save.text = insertedText[0]
        second_name_save.text = insertedText[1]
        third_name_save.text = insertedText[2]
        age_save.text = insertedText[3]
        hobby_save.text = insertedText[4]

        val sharedPr = getSharedPreferences("sharedPr", Context.MODE_PRIVATE)
        val editor = sharedPr.edit()
        editor.apply {
            putString("First", insertedText[0])
            putString("Second", insertedText[1])
            putString("Third", insertedText[2])
            putString("Age", insertedText[3])
            putString("Hobby", insertedText[4])
        }.apply()

        Toast.makeText(this, "Сохранено!", Toast.LENGTH_SHORT).show()
    }

    private fun loadData() {
        val sharedPr = getSharedPreferences("sharedPr", Context.MODE_PRIVATE)
        first_name_save.text = sharedPr.getString("First", null)
        second_name_save.text = sharedPr.getString("Second", null)
        third_name_save.text = sharedPr.getString("Third", null)
        age_save.text = sharedPr.getString("Age", null)
        hobby_save.text = sharedPr.getString("Hobby", null)
    }
}